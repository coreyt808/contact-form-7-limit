<?php
/*
Plugin Name: Contact Form 7 Limit
Description: Limit the number of times a contact form can email you.
Author: Webhead LLC
Version: 1.0
Author URI: http://webheadcoder.com/
*/

define( 'CF7L_DEFAULT_LIMIT', 2 );
define( 'CF7L_DEFAULT_SECONDS', 60 );
define( 'CF7L_DEFAULT_BY_IP', 0 );
define( 'CF7L_DEFAULT_HIDE_CAPTCHA', 0 );


function cf7_check_cf7_is_acive() {
    if ( ! defined( 'WPCF7_VERSION' ) ) {
        add_action( 'admin_notices', 'cf7l_inactive_notice' );
        return;
    }   
}
add_action('plugins_loaded', 'cf7_check_cf7_is_acive');

/**
 * Displays an admin notice advising the admin to install & activate Contact Form 7.
 *
 */
function cf7l_inactive_notice() { ?>
    <div id="message" class="error">
        <p><?php printf( __( '%sThe Contact Form 7 plugin is inactive.%s The %sContact Form 7 plugin%s must be active for the Contact Form 7 Limit plugin to work. Please %sinstall & activate Contact Form 7%s', 'cf7l' ), '<strong>', '</strong>', '', '', '<a href="' . admin_url( 'plugins.php' ) . '">', '&nbsp;&raquo;</a>' ); ?></p>
    </div>
<?php
}

/**
 * Initialize plugin
 */
function cf7l_init() {

    if (is_admin()) {
        require_once('options-page.php');
    }

    do_action('cf7l_init');

}
add_action('init', 'cf7l_init');

/**
 * Add filter to clear form if this is a multistep form.
 */
function cf7l_maybe_skip_mail($cf7) {
    if ( $cf7->skip_mail )
        return;

    //determine if we should skip mail
    if ( cf7l_is_over_limit(1) ) {
        $cf7->skip_mail = true;

        do_action_ref_array( 'cf7l_over_limit', array( &$cf7, $submitted_count ) );
    }

}
add_action( 'wpcf7_before_send_mail', 'cf7l_maybe_skip_mail', 10 );

/**
 * Check if submissions are over the limit.
 */
function cf7l_is_over_limit($is_submitting = 0) {
    $options = get_option('cf7l_options');
    $limit = cf7l_options( 'limit', CF7L_DEFAULT_LIMIT, $options);
    $seconds = cf7l_options( 'seconds', CF7L_DEFAULT_SECONDS, $options);
    $by_ip = cf7l_options( 'by_ip', CF7L_DEFAULT_BY_IP, $options);
    $submitted_count = $is_submitting ? 1 : 0;

    $cf7l_submissions = get_option('_cf7l_submissions');
    $now = time();
    $last = 0;
    $ip = $_SERVER['REMOTE_ADDR'];
    if ( !filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) ) {
        $ip = "0";
    }
    //go through the array backwards
    for ( $i = count($cf7l_submissions) - 1; $i >= 0; $i-- ) {
        $submitted = $cf7l_submissions[$i];
        if ( ( $submitted['time'] + $seconds ) < $now ) {
            $last = $i;
            break;
        }
        if ($by_ip) {
            if ( $submitted['ip'] == $ip ) {
                $submitted_count++;
            }
        }
        else {
            $submitted_count++;
        }
    }
    //get rid of old time
    if ($last > 0) {
        $cf7l_submissions = array_slice($cf7l_submissions, $last);
    }
    if ($is_submitting) {
        $cf7l_submissions[] = array( 'time' => $now, 'ip' => $ip );   
    }
    if ( $last > 0 || $is_submitting ) {
        update_option('_cf7l_submissions', $cf7l_submissions);   
    }

    return ( $submitted_count > $limit );
}


/**
 * Get option
 */
function cf7l_options($name, $default='', $options = false) {
    if (empty($options)) {
        $options = get_option('cf7l_options'); 
    }

    if (!empty($options) && !empty($options[$name])) {
        $ret = $options[$name];
    }
    else {
        $ret = $default;
    }
    return $ret;
}
