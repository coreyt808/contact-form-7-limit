=== Plugin Name ===
Contributors: webheadllc
Tags: contact form 7 form, store form, contact form, captcha, hackers, bots
Requires at least: 3.4.1
Tested up to: 3.8
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Prevent massive emails from being sent through your contact from via bots or just some person with nothing better to do with their life.

== Description ==

After receiving about 25 emails in a span of 1 minute a couple times over the past few months, I decided to write a plugin to limit the number of times a contact form 7 can be submitted within a set time span.

**Usage**

1. Create a contact form 7 form as you normally would.

1. After activating this plugin, go to Settings -> Contact Form 7 Limit.

1. Set the number of times per timespan you want to allow the Contact Form 7 to send you an email.


== Changelog ==

= 1.0 =
Initial release.
