<?php

/**
 *  Add menu page
 */
function cf7l_options_add_page() {

    $hook = add_options_page( 'Contact Form 7 Limit Options', // Page title
                      'Contact Form 7 Limit', // Label in sub-menu
                      cf7l_options_capability(), // capability
                      'cf7l-options', // page identifier 
                      'cf7l_options_do_page' ); // call back function name
}
add_action('admin_menu', 'cf7l_options_add_page');

/**
 * Init plugin options to white list our options
 */
function cf7l_options_init(){
    register_setting( 'cf7l_options_options', 'cf7l_options', 'cf7l_options_validate' );
}
add_action('admin_init', 'cf7l_options_init' );

/**
 * Return the capability needed to access the options page.
 */
function cf7l_options_capability() {
    if ( defined( 'WPCF7_ADMIN_READ_WRITE_CAPABILITY' ) ) {
        $capability = WPCF7_ADMIN_READ_WRITE_CAPABILITY;
    }
    else {
        $capability = 'publish_pages';
    }
    return apply_filters( 'cf7l_options_capability', $capability );
}

/**
 * Draw the menu page itself
 */
function cf7l_options_do_page() {
    if ( !current_user_can( cf7l_options_capability() ) ) { 
     wp_die( __( 'You do not have sufficient permissions to access this page.' ) ); 
    } 
    global $wp_rewrite;
    //Call flush_rules() as a method of the $wp_rewrite object
    $wp_rewrite->flush_rules();
    ?>
    <div class="wrap">
        <h2>Contact Form 7 Limit Options</h2>
        <form method="post" action="options.php">
            <?php settings_fields('cf7l_options_options'); ?>
            <?php $options = get_option('cf7l_options');?>
            <table class="form-table">
                <tr valign="top"><td scope="row">Limit 
                        <input type="text" name="cf7l_options[limit]" style="width:50px;" value="<?php echo esc_attr(cf7l_options('limit', CF7L_DEFAULT_LIMIT, $options) );?>">
                        emails every 
                        <input type="text" name="cf7l_options[seconds]" style="width:50px;"  value="<?php echo esc_attr(cf7l_options('seconds', CF7L_DEFAULT_SECONDS, $options) );?>">
                        seconds.
                    </td>
                </tr> 
                <tr valign="top"><td scope="row"><input type="checkbox" name="cf7l_options[by_ip]" value="1" <?php checked( cf7l_options('by_ip', CF7L_DEFAULT_BY_IP, $options) ); ?> /> Be more less strict and limit per IP address
                </td></tr>
                
            </table>
            <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
            </p>
        </form>
    </div>
    <?php   
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function cf7l_options_validate($input) {
    global $wp_settings_errors;

    if ( !isset( $input['by_ip'] ) ) {
        $input['by_ip'] = 0;
    }

    return $input;
}


